<!DOCTYPE html>
    <html>
    <head>
        <title>Product add</title>
        <link rel="stylesheet" href="style.css">
    </head>

<body>
        <div class="PrAd">
			<div class="left_container">
				<h2>Product Add</h2>
			</div>
			<div class="right_container">
				<button class="button" type="button" onclick="submitForm();">Save</button>
			</div>
            <hr>
        </div>
        <div class="form">
            <form action="created_product.php" method="POST" id="product_add_form">
                <label for="SKU">SKU</label>
                <input type="text" id="formbox_sku" name="SKU"><br>
                <label for="Name">Name</label>
                <input type="text" id="formbox_name" name="Name"><br>
                <label for="Price">Price</label>
                <input type="number" id="formbox_price" name="Price"><br>
                <label>Type Switcher</label>
                <select id="type" name="product" onChange="prodType(this.value);">
                    <option value="">Type Switcher</option>
                    <option value="Acme Disc">Acme Disc</option>
                    <option value="War and Peace">War and Peace</option>
                    <option value="Chair">Chair</option>
                </select>
                
                <div class="fieldbox" id="acme_disc_attributes">
                  <label>Size</label>
                  <input type="number" name="size">
                  <p>"Please provide size in MB format"</p>
                </div>
                
                <div class="fieldbox" id="war_peace_attributes">
                  <label>Weight</label>
                  <input type="number" name="weight">
                  <p>"Please provide weight in KG format"</p>
                </div>
                
                <div class="fieldbox" id="chair_attributes">
                  <label>Height</label>
                  <input type="text" name="height"><br>
                  <label>Width</label>
                  <input type="text" name="width"><br>
                  <label>Lenght</label>
                  <input type="text" name="length"><br>
                  <p>"Please provide dimensions  in HxWxL format"</p>
                </div>
            </form>
        </div>

    	<script src="javascript.js"></script>
		<script>
			function submitForm(){
				document.getElementById("product_add_form").submit();
			}
		</script>
    </body>
    </html>
