<?php 
include_once "databasehost.php";
$sql = "SELECT * FROM product";
$result = mysqli_query($conn,$sql);
$resultCheck = mysqli_num_rows($result);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Sales</title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<div class="PrAd">
			<div class="left_container">
				<h2>Product List</h2>
			</div>
			<?php
				if($resultCheck>0){
					?>
					<div class="right_container">
						<button class="button">Apply</button>
					</div>
				<?php
				}
				?>
			
            <hr>
        </div>

<?php
				if($resultCheck>0){
					?>
					<div class="row">
						<?php
							while($row = mysqli_fetch_assoc($result)) {
						?>		
						<div class="column">
							<div class="card">
								<div class="label">
									<label class="container">
										<input type="checkbox">
									</label>
								</div>
								
								<div>
									<h3><?php echo $row['product_SKU'];?></h3>
									<div><?php echo $row['product_name'];?></div>
									<div><?php echo $row['product_price'];?></div>
									<div><?php echo $row['product_character'];?></div>
								</div>


							</div>
						</div>
						<?php
							}
						?>	
					</div>
				<?php
				}else{
					echo '<p>No product(s) found.</p>';
				}
?>				
	
</body>
</html>